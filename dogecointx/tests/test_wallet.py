import unittest

from bitcointx.tests.test_wallet import test_address_implementations

from bitcointx import ChainParams
from bitcointx.wallet import CCoinAddress, CCoinAddressError

from bitcointx.core import Hash160
from bitcointx.core.script import CScript, OP_0

from dogecointx import DogecoinMainnetParams
from dogecointx.wallet import P2PKHDogecoinAddress, P2PKHDogecoinTestnetAddress, P2PKHDogecoinRegtestAddress


class Test_DogecoinAddress(unittest.TestCase):

    def test_address_implementations(self, paramclasses=None):
        test_address_implementations(self, paramclasses=[DogecoinMainnetParams])

    def test_p2pk(self):
        with ChainParams('dogecoin'):
            a = CCoinAddress('DJcczDr7oNvfj5qP17Qa7p9ZUNTfnYYDJC')
            self.assertIsInstance(a, P2PKHDogecoinAddress)

        with ChainParams('dogecoin/testnet'):
            a = CCoinAddress('nhfgiEb2jMPPc4Qa2w42NDjriEqxmHeSRG')
            self.assertIsInstance(a, P2PKHDogecoinTestnetAddress)

        with ChainParams('dogecoin/regtest'):
            a = CCoinAddress('mtzUk1zTJzTdyC8Pz6PPPyCHTEL5RLVyDJ')
            self.assertIsInstance(a, P2PKHDogecoinRegtestAddress)

from typing import Type, List

from bitcointx import get_current_chain_params
from bitcointx.util import dispatcher_mapped_list, ClassMappingDispatcher
from bitcointx.wallet import (
    WalletCoinClassDispatcher, WalletCoinClass,
    CCoinAddress, P2SHCoinAddress, P2WSHCoinAddress,
    P2PKHCoinAddress, P2WPKHCoinAddress,
    CBase58CoinAddress,
    CCoinKey, CCoinExtKey, CCoinExtPubKey,
    T_CBase58DataDispatched
)
from .core import CoreDogecoinClassDispatcher


class WalletDogecoinClassDispatcher(WalletCoinClassDispatcher,
                                    depends=[CoreDogecoinClassDispatcher]):
    ...


class WalletDogecoinTestnetClassDispatcher(WalletDogecoinClassDispatcher):
    ...


class WalletDogecoinRegtestClassDispatcher(WalletDogecoinClassDispatcher):
    ...


class WalletDogecoinClass(WalletCoinClass,
                          metaclass=WalletDogecoinClassDispatcher):
    ...


class WalletDogecoinTestnetClass(
    WalletDogecoinClass, metaclass=WalletDogecoinTestnetClassDispatcher
):
    ...


class WalletDogecoinRegtestClass(
    WalletDogecoinClass, metaclass=WalletDogecoinRegtestClassDispatcher
):
    ...


class CDogecoinAddress(CCoinAddress, WalletDogecoinClass):
    ...


class CDogecoinTestnetAddress(CCoinAddress, WalletDogecoinTestnetClass):
    ...


class CDogecoinRegtestAddress(CCoinAddress, WalletDogecoinRegtestClass):
    ...


class CBase58DogecoinAddress(CBase58CoinAddress, CDogecoinAddress):
    @classmethod
    def base58_get_match_candidates(cls: Type[T_CBase58DataDispatched]
                                    ) -> List[Type[T_CBase58DataDispatched]]:
        assert isinstance(cls, ClassMappingDispatcher)
        candidates = dispatcher_mapped_list(cls)
        if P2SHDogecoinAddress in candidates\
                and get_current_chain_params().allow_legacy_p2sh:
            return [P2SHDogecoinLegacyAddress] + candidates
        return super(CBase58DogecoinAddress, cls).base58_get_match_candidates()


class CBase58DogecoinTestnetAddress(CBase58CoinAddress,
                                    CDogecoinTestnetAddress):
    ...


class CBase58DogecoinRegtestAddress(CBase58CoinAddress,
                                    CDogecoinRegtestAddress):
    ...


class P2SHDogecoinAddress(P2SHCoinAddress, CBase58DogecoinAddress):
    base58_prefix = bytes([22])


class P2SHDogecoinLegacyAddress(P2SHCoinAddress, CBase58DogecoinAddress,
                                variant_of=P2SHDogecoinAddress):
    base58_prefix = bytes([22])


class P2PKHDogecoinAddress(P2PKHCoinAddress, CBase58DogecoinAddress):
    base58_prefix = bytes([30])


class P2SHDogecoinTestnetAddress(P2SHCoinAddress,
                                 CBase58DogecoinTestnetAddress):
    base58_prefix = bytes([196])


class P2SHDogecoinTestnetLegacyAddress(P2SHCoinAddress,
                                       CBase58DogecoinTestnetAddress,
                                       variant_of=P2SHDogecoinTestnetAddress):
    base58_prefix = bytes([196])


class P2PKHDogecoinTestnetAddress(P2PKHCoinAddress,
                                  CBase58DogecoinTestnetAddress):
    base58_prefix = bytes([113])


class P2SHDogecoinRegtestAddress(P2SHCoinAddress,
                                 CBase58DogecoinRegtestAddress):
    base58_prefix = bytes([196])


class P2SHDogecoinRegtestLegacyAddress(P2SHCoinAddress,
                                       CBase58DogecoinRegtestAddress,
                                       variant_of=P2SHDogecoinRegtestAddress):
    base58_prefix = bytes([196])


class P2PKHDogecoinRegtestAddress(P2PKHCoinAddress,
                                  CBase58DogecoinRegtestAddress):
    base58_prefix = bytes([111])


class CDogecoinKey(CCoinKey, WalletDogecoinClass):
    base58_prefix = bytes([158])


class CDogecoinTestnetKey(CCoinKey, WalletDogecoinTestnetClass):
    base58_prefix = bytes([241])


class CDogecoinRegtestKey(CCoinKey, WalletDogecoinRegtestClass):
    base58_prefix = bytes([239])


class CDogecoinExtPubKey(CCoinExtPubKey, WalletDogecoinClass):
    base58_prefix = b'\x02\xFA\xCA\xFD'


class CDogecoinExtKey(CCoinExtKey, WalletDogecoinClass):
    base58_prefix = b'\x02\xFA\xC3\x98'


class CDogecoinTestnetExtPubKey(CCoinExtPubKey, WalletDogecoinTestnetClass):
    base58_prefix = b'\x04\x35\x87\xCF'


class CDogecoinTestnetExtKey(CCoinExtKey, WalletDogecoinTestnetClass):
    base58_prefix = b'\x04\x35\x83\x94'


class CDogecoinRegtestExtPubKey(CCoinExtPubKey, WalletDogecoinRegtestClass):
    base58_prefix = b'\x04\x35\x87\xCF'


class CDogecoinRegtestExtKey(CCoinExtKey, WalletDogecoinRegtestClass):
    base58_prefix = b'\x04\x35\x83\x94'


__all__ = (
    'CDogecoinAddress',
    'CDogecoinTestnetAddress',
    'CBase58DogecoinAddress',
    'P2SHDogecoinAddress',
    'P2SHDogecoinLegacyAddress',
    'P2PKHDogecoinAddress',
    'P2WSHDogecoinAddress',
    'P2WPKHDogecoinAddress',
    'CBase58DogecoinTestnetAddress',
    'P2SHDogecoinTestnetAddress',
    'P2PKHDogecoinTestnetAddress',
    'P2WSHDogecoinTestnetAddress',
    'P2WPKHDogecoinTestnetAddress',
    'CDogecoinKey',
    'CDogecoinExtKey',
    'CDogecoinExtPubKey',
    'CDogecoinTestnetKey',
    'CDogecoinTestnetExtKey',
    'CDogecoinTestnetExtPubKey',
    'CDogecoinRegtestKey',
    'CDogecoinRegtestExtKey',
    'CDogecoinRegtestExtPubKey',
)
